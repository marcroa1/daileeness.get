package com.marcroa.daileeness;

import static com.marcroa.daileeness.DaileenessApp.ALARM_ID;
import static com.marcroa.daileeness.DaileenessApp.SHARED_PREFERENCES_KEY;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.switchmaterial.SwitchMaterial;
import com.marcroa.daileeness.notify.DailyNotificationReceiver;

import java.util.Calendar;
import java.util.Objects;

public class SettingsActivity extends AppCompatActivity {

    public static final String SETTINGS_key = "dailyeenessSettings";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        SwitchMaterial notificationSwitch = findViewById(R.id.notificationSwitch);
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);


        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent notificationAlarm = new Intent(this, DailyNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, ALARM_ID, notificationAlarm, 0);

        notificationSwitch.setChecked(sharedPreferences.getBoolean(SETTINGS_key, false));

        notificationSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {

                Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, 23);
                c.set(Calendar.MINUTE, 45);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MILLISECOND, 0);
                if (Calendar.getInstance().after(c)) c.add(Calendar.DATE, 1);

                alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), 8 * AlarmManager.INTERVAL_HOUR, pendingIntent);
                sharedPreferences.edit().putBoolean(SETTINGS_key, true).apply();
                Toast.makeText(SettingsActivity.this, "Notifications are turned on", Toast.LENGTH_SHORT).show();
            } else {
                alarmManager.cancel(pendingIntent);
                sharedPreferences.edit().putBoolean(SETTINGS_key, false).apply();
                Toast.makeText(SettingsActivity.this, "Notifications are turned off", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, MainActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }
}
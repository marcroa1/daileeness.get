package com.marcroa.daileeness.domain;

import com.marcroa.daileeness.R;

public enum NessType {

    // Food related types
    COFFEE(R.mipmap.ic_coffee),
    DRINK(R.mipmap.ic_drink),
    HAMBURGER(R.mipmap.ic_hamburger),
    ICE_CREAM(R.mipmap.ic_ice_cream),
    PASTA(R.mipmap.ic_pasta),
    PIZZA(R.mipmap.ic_pizza),
    RESTAURANT(R.mipmap.ic_restaurant),

    // Other types
    GIFT(R.mipmap.ic_gift),
    GAME(R.mipmap.ic_game),
    LOVE(R.mipmap.ic_love),
    QUESTION(R.mipmap.ic_question),
    MUSIC(R.mipmap.ic_spotify),
    SUN(R.mipmap.ic_sun),
    TASK(R.mipmap.ic_task),
    TRIP(R.mipmap.ic_trip),
    VIDEO(R.mipmap.ic_youtube);


    private int iconId;

    NessType(int iconId) {this.iconId = iconId;}

    public int getIconId() {
        return iconId;
    }
}

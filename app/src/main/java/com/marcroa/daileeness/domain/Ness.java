package com.marcroa.daileeness.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Ness {
    private final int id;
    private final NessType type;

    private final String text;
    private final String link;
    private int order;
    private String dateOfOpening;
    private boolean isOpened;

    public Ness(int id, NessType type, String text, String link) {
        this.id = id;
        this.type = type;
        this.text = text;
        this.link = link;
        this.dateOfOpening = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        this.isOpened = id == 0;
        this.order = 69;
    }

    public int getId() {
        return id;
    }

    public NessType getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public String getLink() {
        return link;
    }

    public LocalDate getDateOfOpening() {
        return LocalDate.parse(dateOfOpening, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public boolean isOpened() {
        return isOpened;
    }

    public int getOrder() {
        return order;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ness ness = (Ness) o;
        return id == ness.id;
    }

    public Ness open(int order) {
        this.order = order;
        dateOfOpening = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE);
        isOpened = true;
        return this;
    }
}

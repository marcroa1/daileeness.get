package com.marcroa.daileeness.notify;

import static com.marcroa.daileeness.DaileenessApp.ALARM_ID;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;

public class WakeUpNotificationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") || intent.getAction().equals("android.intent.action.REBOOT")) {
            Intent myIntent = new Intent(context, DailyNotificationReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, ALARM_ID, myIntent, 0);
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

            Calendar c = Calendar.getInstance();
            c.set(Calendar.HOUR_OF_DAY, 23);
            c.set(Calendar.MINUTE, 45);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            if (Calendar.getInstance().after(c)) c.add(Calendar.DATE, 1);

            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, c.getTimeInMillis(), 8 * AlarmManager.INTERVAL_HOUR, pendingIntent);
        }
    }
}

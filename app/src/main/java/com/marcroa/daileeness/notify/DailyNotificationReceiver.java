package com.marcroa.daileeness.notify;


import static com.marcroa.daileeness.DaileenessApp.CHANNEL_ID;
import static com.marcroa.daileeness.DaileenessApp.NOTIFICATION_ID;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.marcroa.daileeness.MainActivity;
import com.marcroa.daileeness.R;
import com.marcroa.daileeness.domain.Ness;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class DailyNotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    CHANNEL_ID,
                    "Daileeness Notifications",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.enableVibration(true);
            channel.enableVibration(true);
            channel.enableLights(true);

            NotificationManager manager = context.getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);

        }
        List<String> messages = new ArrayList<>();
        messages.add("Van egy új napi üzid :)");
        messages.add("Valami lapul itt, lehet meg kéne nézned, hogy miaz ;)");
        messages.add("Ma vajon mit sorsol ki neked a gép? :O");
        messages.add("Biró Edinát keresem, hoztam neki egy üzenetet!");
        messages.add("Van egy üzid drágám, nézd meg gyorsan mi az :)");
        messages.add("Önnek egy új olvasatlan üzenete van.");
        messages.add("Hopp! Valami érkezett, nézd meg gyorsan miaz :)");
        messages.add("Valami nagyon jó érkezett ma ide neked :)\nNem, nem azért tudom mert beleolvastam...");


        Ness nessToSet = MainActivity.getNessToSet(context);
        NotificationManagerCompat managerCompat = NotificationManagerCompat.from(context);
        if (!nessToSet.isOpened()) {
            String message = messages.get(new Random().nextInt(messages.size()));
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);
            builder.setContentTitle("New daily message!")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setSmallIcon(R.drawable.ic_notification_icon)
                    .setTicker("New daily message!")
                    .setAutoCancel(true)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setContentIntent(pendingIntent);
            managerCompat.notify(NOTIFICATION_ID, builder.build());
        }
    }
}

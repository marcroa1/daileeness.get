package com.marcroa.daileeness.data;

import static com.marcroa.daileeness.DaileenessApp.SHARED_PREFERENCES_KEY;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.marcroa.daileeness.domain.Ness;
import com.marcroa.daileeness.domain.NessType;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class NessHandler {
    private static final String ALL_NESS_KEY = "remainingNesses";
    private static final String LAST_ORDER_NUMBER_KEY = "lastOrderNumber";
    private static final String LAST_OPENED_NESS = "lastOpenedNess";
    private static NessHandler instance;
    private final SharedPreferences sharedPreferences;
    private final Ness defaultNess = new Ness(0, NessType.COFFEE, "Itt a vége, fuss el véle", "");

    private NessHandler(Context context) {
        sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
        if (getAllNesses() == null) initializeNesses();
        if (getLastOrderNumber() == -1) initializeLastOrderNumber();
    }

    private boolean initializeNesses() {
        List<Ness> nesses = new ArrayList<>();
        Gson gson = new Gson();
        //Music
        nesses.add(new Ness(1, NessType.MUSIC, "Music sample message", "https://open.spotify.com/track/6l78mUZ86r0NrfRgni0LuM?si=46f675df7a0a45eb"));
        //Video
        nesses.add(new Ness(22, NessType.VIDEO, "Video example", "https://youtu.be/-50NdPawLVY"));
        //Food
        nesses.add(new Ness(37, NessType.PIZZA, "Food coupon example", ""));
        //Gaming
        nesses.add(new Ness(52, NessType.GAME, "Game coupon example", ""));
        //Trip
        nesses.add(new Ness(61, NessType.TRIP, "Trip coupon example", ""));
        //Question
        nesses.add(new Ness(70, NessType.QUESTION, "Question example", ""));
        //Task
        nesses.add(new Ness(80, NessType.TASK, "Task example", ""));
        //Gift
        nesses.add(new Ness(96, NessType.GIFT, "Gift Coupon", ""));
        //Sun
        nesses.add(new Ness(321, NessType.SUN, "Napozás", ""));
        //Love
        nesses.add(new Ness(120, NessType.LOVE, "Love example", ""));

        return sharedPreferences.edit()
                .putString(ALL_NESS_KEY, gson.toJson(nesses))
                .commit();
    }

    private boolean initializeLastOrderNumber() {
        return setLastOrderNumber(0);
    }

    private boolean setLastOrderNumber(int orderNumber) {
        return sharedPreferences.edit().putInt(LAST_ORDER_NUMBER_KEY, orderNumber).commit();
    }

    public static synchronized NessHandler getInstance(Context context) {
        if (instance == null) instance = new NessHandler(context);
        return instance;
    }

    public int getLastOrderNumber() {
        return sharedPreferences.getInt(LAST_ORDER_NUMBER_KEY, -1);
    }

    public List<Ness> getAllNesses() {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<Ness>>() {
        }.getType();
        return gson.fromJson(sharedPreferences.getString(ALL_NESS_KEY, null), type);
    }

    public List<Ness> getUnopenedNesses() {
        List<Ness> allNesses = getAllNesses();
        List<Ness> unopenedNesses = new ArrayList<>();
        if (allNesses != null)
            unopenedNesses = allNesses.stream().filter(ness -> !ness.isOpened()).collect(Collectors.toList());
        if (unopenedNesses.isEmpty()) unopenedNesses.add(defaultNess);
        return unopenedNesses;
    }

    public List<Ness> getOpenedNesses() {
        List<Ness> allNesses = getAllNesses();
        List<Ness> openedNesses = new ArrayList<>();
        if (allNesses != null)
            openedNesses = allNesses.stream().filter(Ness::isOpened).collect(Collectors.toList());
        if (openedNesses.isEmpty()) openedNesses.add(defaultNess);
        return openedNesses;
    }

    public Ness getNessById(int id) {
        List<Ness> allNesses = getAllNesses();
        Optional<Ness> optionalNess = Optional.empty();
        if (allNesses != null)
            optionalNess = allNesses.stream().filter(ness -> ness.getId() == id).findFirst();
        return optionalNess.orElse(defaultNess);
    }

    public boolean openNess(Ness ness) {
        int order = getLastOrderNumber();
        if (order != -1) {
            List<Ness> allNesses = getAllNesses();
            if (allNesses != null) {
                List<Ness> changedNesses = allNesses.stream().filter(n -> !n.equals(ness)).collect(Collectors.toList());
                changedNesses.add(ness.open(order + 1));
                Gson gson = new Gson();

                return setLastOpenedNess(ness) && sharedPreferences.edit()
                        .remove(ALL_NESS_KEY)
                        .putString(ALL_NESS_KEY, gson.toJson(changedNesses))
                        .commit();
            }
        }
        return false;
    }

    private boolean setLastOpenedNess(Ness ness) {
        Gson gson = new Gson();
        return setLastOrderNumber(ness.getOrder()) && sharedPreferences.edit()
                .remove(LAST_OPENED_NESS)
                .putString(LAST_OPENED_NESS, gson.toJson(ness))
                .commit();
    }

    public Ness getLastOpenedNess() {
        Gson gson = new Gson();
        Type type = new TypeToken<Ness>(){}.getType();
        String test = sharedPreferences.getString(LAST_OPENED_NESS, null);
        return gson.fromJson(sharedPreferences.getString(LAST_OPENED_NESS, null), type);
    }

}

package com.marcroa.daileeness.adapter;

import static com.marcroa.daileeness.DaileenessApp.NESS_EXTRA_ID;
import static com.marcroa.daileeness.DaileenessApp.ORIGIN_EXTRA_ID;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.marcroa.daileeness.NessActivity;
import com.marcroa.daileeness.R;
import com.marcroa.daileeness.data.NessHandler;
import com.marcroa.daileeness.domain.Ness;

import java.util.ArrayList;
import java.util.List;

public class NessRecyclerViewAdapter extends RecyclerView.Adapter<NessRecyclerViewAdapter.ViewHolder> {
    private List<Ness> nesses = new ArrayList<>();
    private final Context context;

    public NessRecyclerViewAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ness_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Ness currentNess = nesses.get(position);
        Glide.with(context)
                .asBitmap()
                .load(currentNess.getType().getIconId())
                .into(holder.image);
        if (currentNess.isOpened()) {
            holder.descriptionText.setText(currentNess.getText());
            holder.orderNumber.setText(String.valueOf(currentNess.getOrder()));
            holder.parent.setOnClickListener(v -> {
                Intent intent = new Intent(context, NessActivity.class);
                intent.putExtra(NESS_EXTRA_ID, currentNess.getId());
                context.startActivity(intent);
            });
        } else {
            holder.openButton.setVisibility(View.VISIBLE);
            holder.descriptionText.setVisibility(View.GONE);
            holder.orderNumber.setText(String.valueOf(NessHandler.getInstance(context).getLastOrderNumber()+1));
            holder.openButton.setOnClickListener(v -> {
                Intent intent = new Intent(context, NessActivity.class);
                intent.putExtra(NESS_EXTRA_ID, currentNess.getId());
                intent.putExtra(ORIGIN_EXTRA_ID, true);
                if (NessHandler.getInstance(context).openNess(currentNess)) context.startActivity(intent);
                else Toast.makeText(context, "Failed to open item, try again", Toast.LENGTH_SHORT).show();
            });
        }

    }

    @Override
    public int getItemCount() {
        return nesses.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    public void setNesses(List<Ness> nesses) {
        this.nesses = nesses;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView orderNumber;
        private final TextView descriptionText;
        private final ImageView image;
        private final Button openButton;
        private final CardView parent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            orderNumber = itemView.findViewById(R.id.orderNumber);
            descriptionText = itemView.findViewById(R.id.descriptionText);
            image = itemView.findViewById(R.id.smallImage);
            openButton = itemView.findViewById(R.id.openButton);
            parent = itemView.findViewById(R.id.parent);
        }
    }
}

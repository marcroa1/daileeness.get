package com.marcroa.daileeness;

import static com.marcroa.daileeness.DaileenessApp.ALARM_ID;
import static com.marcroa.daileeness.DaileenessApp.NOTIFICATION_ID;
import static java.time.temporal.ChronoUnit.DAYS;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.marcroa.daileeness.adapter.NessRecyclerViewAdapter;
import com.marcroa.daileeness.data.NessHandler;
import com.marcroa.daileeness.domain.Ness;
import com.marcroa.daileeness.notify.DailyNotificationReceiver;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ProgressBar progressBar;
    RecyclerView nessRecyclerView;
    private final NessRecyclerViewAdapter adapter = new NessRecyclerViewAdapter(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Cancel notification if there is any
        NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);

        initializeViews();

        nessRecyclerView.setAdapter(adapter);
        nessRecyclerView.setLayoutManager(new FixedLinearLayoutManager(this));

        Ness activeNess = getNessToSet(this);
        if (activeNess.getId() == 0) {
            cancelNotifications();
        }
        adapter.setNesses(Collections.singletonList(activeNess));
        progressBar.setVisibility(View.GONE);
        optimiseBattery();
    }

    private void cancelNotifications() {
        Intent notificationAlarm = new Intent(this, DailyNotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, ALARM_ID, notificationAlarm, 0);
        ((AlarmManager) getSystemService(ALARM_SERVICE)).cancel(pendingIntent);
    }

    private void initializeViews() {
        progressBar = findViewById(R.id.progressBar);
        nessRecyclerView = findViewById(R.id.simpleNessRecView);
    }

    private void optimiseBattery() {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivity(intent);
            }
        }
    }

    public static Ness getNessToSet(Context context) {
        NessHandler nessHandler = NessHandler.getInstance(context);
        Ness lastNess = nessHandler.getLastOpenedNess();
        if (lastNess != null) {
            if (DAYS.between(lastNess.getDateOfOpening(), LocalDate.now()) < 1) {
                return lastNess;
            }
        }

        List<Ness> unopenedNesses = nessHandler.getUnopenedNesses();
        return unopenedNesses.get(new Random().nextInt(unopenedNesses.size()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.aboutMenu:
                Toast.makeText(this, "About selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.showAllMenu:
                Intent allIntent = new Intent(this, OpenedNessActivity.class);
                startActivity(allIntent);
                return true;
            case R.id.settingsMenu:
                Intent settingsIntent = new Intent(this, SettingsActivity.class);
                startActivity(settingsIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class FixedLinearLayoutManager extends LinearLayoutManager {

        public FixedLinearLayoutManager(Context context) {
            super(context);
        }

        @Override
        public boolean canScrollVertically() {
            return false;
        }

        @Override
        public boolean canScrollHorizontally() {
            return false;
        }
    }
}
package com.marcroa.daileeness;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.marcroa.daileeness.adapter.NessRecyclerViewAdapter;
import com.marcroa.daileeness.data.NessHandler;

import java.util.Objects;

public class OpenedNessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opened_ness);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        NessRecyclerViewAdapter adapter = new NessRecyclerViewAdapter(this);
        RecyclerView nessRecyclerView = findViewById(R.id.simpleNessRecView);
        nessRecyclerView.setAdapter(adapter);
        nessRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        adapter.setNesses(NessHandler.getInstance(this).getOpenedNesses());
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent homeIntent = new Intent(this, MainActivity.class);
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }
}
package com.marcroa.daileeness;

import static com.marcroa.daileeness.DaileenessApp.NESS_EXTRA_ID;
import static com.marcroa.daileeness.DaileenessApp.ORIGIN_EXTRA_ID;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.marcroa.daileeness.data.NessHandler;
import com.marcroa.daileeness.domain.Ness;

import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class NessActivity extends AppCompatActivity {

    private TextView openedDate;
    private TextView message;
    private ImageView image;
    private Button linkButton;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy. MMMM dd.");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ness);

        initializeViews();
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        if (intent != null) {
            int nessId = intent.getIntExtra(NESS_EXTRA_ID, -1);
            if (nessId != -1) {
                Ness nessById = NessHandler.getInstance(this).getNessById(nessId);
                openedDate.setText(formatter.format(nessById.getDateOfOpening()));
                message.setText(nessById.getText());
                Glide.with(this)
                        .asBitmap()
                        .load(nessById.getType().getIconId())
                        .into(image);
                String link = nessById.getLink();
                if (link != null && !link.isEmpty()) {
                    linkButton.setOnClickListener(v -> {
                        Uri uri = Uri.parse(nessById.getLink());
                        Intent intent1;
                        if (link.startsWith("mailto:")) {
                            intent1 = new Intent(Intent.ACTION_SEND);
                            intent1.setType("application/octet-stream")
                                    .putExtra(Intent.EXTRA_EMAIL, new String[]{link.split(":")[1]})
                                    .putExtra(Intent.EXTRA_SUBJECT, "Test subject")
                                    .putExtra(Intent.EXTRA_TEXT, "Kedves Marci,\n\n...\n\nA te Edinád.");
                        } else intent1 = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent1);
                    });
                } else linkButton.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initializeViews() {
        openedDate = findViewById(R.id.dateText);
        message = findViewById(R.id.messageText);
        image = findViewById(R.id.imageView);
        linkButton = findViewById(R.id.linkButton);
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(ORIGIN_EXTRA_ID, false)) {
            Intent homeIntent = new Intent(this, MainActivity.class);
            homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(homeIntent);
        }
        super.onBackPressed();
    }
}
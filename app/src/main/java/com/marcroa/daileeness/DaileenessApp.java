package com.marcroa.daileeness;

import android.app.Application;

public class DaileenessApp extends Application {

    public static final String NESS_EXTRA_ID = "nessId";
    public static final String ORIGIN_EXTRA_ID = "originId";
    public static final String CHANNEL_ID = "DaileenessChannel";
    public static final int ALARM_ID = 121166303;
    public static final int NOTIFICATION_ID = 420;
    public static final String SHARED_PREFERENCES_KEY = "daileeness_db_sp";

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
